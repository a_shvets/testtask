﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TestTask.Pages;
using Plugin.Connectivity;
using Acr.UserDialogs;

namespace TestTask
{
    public partial class App : Application
    {
        public static bool IsInternetConnected { get; set; }
        public static ContentPage CurrentPage { get; set; }
        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzY5NTE1QDMxMzgyZTM0MmUzMGZaeGdEYUs5aXlDWVQvRVRPdGhVVmpXZFZtQ2NUMUVSeHNwRmFFU1duWUE9");
            InitializeComponent();
            CheckInternetOnStart();
            CheckInternetContiniously();
            MainPage = new NavigationPage(new AllMemoriesPage());
        }
        public void CheckInternetOnStart()
        {
            IsInternetConnected = CrossConnectivity.Current.IsConnected;
        }
        public void CheckInternetContiniously()
        {
            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
        }

        private void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            IsInternetConnected = e.IsConnected;
            if (!e.IsConnected)
            {
                Toast("No internet connection", System.Drawing.Color.Red);
            }
            else
            {
                Toast("Your internet connection was restored", System.Drawing.Color.Green);
                MessagingCenter.Send(this, "Refresh");

            }
        }
        public static void Toast(string message, System.Drawing.Color color, ToastPosition position = ToastPosition.Bottom)
        {
            ToastConfig toastConfig = new ToastConfig(message);
            toastConfig.SetPosition(position);
            toastConfig.SetDuration(9000);
            toastConfig.SetBackgroundColor(color);
            toastConfig.SetMessageTextColor(System.Drawing.Color.White);
            UserDialogs.Instance.Toast(toastConfig);
        }
        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
