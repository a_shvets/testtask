﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.Services
{
    public class ApiServices
    {
        public async Task<HttpResponseMessage> GetStories()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.GetAsync("https://back-dev.awj.aegas.it/api/client-experience");
            return response;
        }
        public async Task<HttpResponseMessage> GetStory(string IdStory)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.GetAsync("https://back-dev.awj.aegas.it/api/client-experience/"+IdStory);
            return response;
        }


    }
}
