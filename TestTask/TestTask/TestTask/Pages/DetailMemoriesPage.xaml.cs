﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestTask.Models;
using TestTask.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTask.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailMemoriesPage : ContentPage
    {
        string idStory;
        public ApiServices apiServices = new ApiServices();
        public ICommand GetStoryCommand
        {
            get
            {
                return new Command(async () =>
                {
                    try
                    {
                        var responseMessage = await apiServices.GetStory(idStory);
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            string json = await responseMessage.Content.ReadAsStringAsync();
                            var stories = JsonConvert.DeserializeObject<StoryModel> (json);
                            toptitle.Text = stories.Title;
                            topnote.Text = $"\"{stories.Note}\"";
                            topimage.Source = stories.ImageUrl;
                            List<StoryVMModel> storyVMModels = new List<StoryVMModel>();
                            foreach(var item in stories.Sections)
                            {
                                var story = new StoryVMModel();
                                switch (item.SectionType)
                                {
                                    case "Photo":
                                        story.Images = item.Section.Url;
                                        break;
                                    case "Day":
                                        story.MainText = item.Section.Text;
                                        story.Title = item.Section.Title;
                                        story.DayNumber = item.Section.Days;
                                        story.IsDay = true;
                                        break;
                                    case "Quote":
                                        story.QuoteText = item.Section.Text;
                                        break;
                                }
                                storyVMModels.Add(story);
                            }
                            strorylist.ItemsSource = null;
                            strorylist.ItemsSource = storyVMModels;
                        }
                        else
                        {
                            var message = responseMessage.Content.ReadAsStringAsync().Result;
                            await DisplayAlert("Story", message, "OK");
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                });
            }
        }
        public DetailMemoriesPage(string IdStory)
        {
            InitializeComponent();
            App.CurrentPage = this;
            idStory = IdStory;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetStoryCommand.Execute(null);
            MessagingCenter.Subscribe<App>(this, "Refresh", (sender) => 
            {
                if (App.CurrentPage == this) GetStoryCommand.Execute(null);
            });
        }
    }
}