﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestTask.Models;
using TestTask.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTask.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllMemoriesPage : ContentPage
    {
        public ApiServices apiServices = new ApiServices();
        ObservableCollection<AllStoriesModel> allStories = new ObservableCollection<AllStoriesModel>();
        public ICommand GetAllStoriesCommand
        {
            get
            {
                return new Command(async () =>
                {
                    try
                    {
                        if (!App.IsInternetConnected) 
                        {
                            App.Toast("No internet connection", System.Drawing.Color.Red); 
                            return; 
                        }
                        var responseMessage = await apiServices.GetStories();
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            string json = await responseMessage.Content.ReadAsStringAsync();
                            var strories = JsonConvert.DeserializeObject<ObservableCollection<AllStoriesModel>>(json);
                            allStories.Clear();
                            if (strories.Count > 0)
                            {
                                foreach (var story in strories)
                                {
                                    allStories.Add(story);

                                }
                                liststories.ItemsSource = null;
                                liststories.ItemsSource = allStories;
                            }
                        }
                        else
                        {

                            var message = responseMessage.Content.ReadAsStringAsync().Result;
                            await DisplayAlert("All stories", message, "OK");
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                });
            }
        }
        public AllMemoriesPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetAllStoriesCommand.Execute(null);
            MessagingCenter.Subscribe<App>(this, "Refresh", (sender) =>
            {
                if (App.CurrentPage == this) GetAllStoriesCommand.Execute(null);
            });
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var model = (AllStoriesModel)(sender as Button)?.BindingContext;
            await Navigation.PushAsync(new DetailMemoriesPage(model.Id));
        }
    }
}