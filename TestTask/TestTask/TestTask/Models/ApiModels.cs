﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TestTask.Models
{
    public class AllStoriesModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private string id;
        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                if (value != id)
                {
                    id = value;
                    PropertyChanged(this, new PropertyChangedEventArgs("Id"));
                }
            }
        }

        private string title;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                if (value != title)
                {
                    title = value;
                    PropertyChanged(this, new PropertyChangedEventArgs("Title"));
                }
            }
        }

        private string previewImageUrl;
        public string PreviewImageUrl
        {
            get
            {
                return previewImageUrl;
            }
            set
            {
                if (value != previewImageUrl)
                {
                    previewImageUrl = value;
                    PropertyChanged(this, new PropertyChangedEventArgs("PreviewImageUrl"));
                }
            }
        }
    }
    public class Section2Model
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string Days { get; set; }
        public string Url { get; set; }
    }

    public class SectionModel
    {
        public string SectionType { get; set; }
        public Section2Model Section { get; set; }
    }

    public class StoryModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public string ImageUrl { get; set; }
        public string PreviewImageUrl { get; set; }
        public List<SectionModel> Sections { get; set; }
    }
    public class StoryVMModel
    {
        public string DayNumber { get; set; }
        public string Title { get; set; }
        public string MainText { get; set; }
        public string Images { get; set; }
        public string QuoteText { get; set; }
        public bool IsDay { get; set; } = false;
    }
}


